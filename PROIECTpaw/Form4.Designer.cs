﻿namespace PROIECTpaw
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form4));
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonFranta = new System.Windows.Forms.RadioButton();
            this.radioButtonRomania = new System.Windows.Forms.RadioButton();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.informatiiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.numarLotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tbDen = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.tbNr = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuThinButton21 = new Bunifu.Framework.UI.BunifuThinButton2();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 19);
            this.label2.TabIndex = 5;
            this.label2.Text = "Denumire produs";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 157);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(158, 19);
            this.label3.TabIndex = 6;
            this.label3.Text = "Numar linie productie";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 243);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 19);
            this.label4.TabIndex = 7;
            this.label4.Text = "Data productie";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonFranta);
            this.groupBox1.Controls.Add(this.radioButtonRomania);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 295);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(412, 109);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tara provenienta";
            // 
            // radioButtonFranta
            // 
            this.radioButtonFranta.AutoSize = true;
            this.radioButtonFranta.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonFranta.Location = new System.Drawing.Point(240, 52);
            this.radioButtonFranta.Name = "radioButtonFranta";
            this.radioButtonFranta.Size = new System.Drawing.Size(75, 23);
            this.radioButtonFranta.TabIndex = 1;
            this.radioButtonFranta.TabStop = true;
            this.radioButtonFranta.Text = "Franta";
            this.radioButtonFranta.UseVisualStyleBackColor = true;
            // 
            // radioButtonRomania
            // 
            this.radioButtonRomania.AutoSize = true;
            this.radioButtonRomania.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonRomania.Location = new System.Drawing.Point(90, 52);
            this.radioButtonRomania.Name = "radioButtonRomania";
            this.radioButtonRomania.Size = new System.Drawing.Size(93, 23);
            this.radioButtonRomania.TabIndex = 0;
            this.radioButtonRomania.TabStop = true;
            this.radioButtonRomania.Text = "Romania";
            this.radioButtonRomania.UseVisualStyleBackColor = true;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.informatiiToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(730, 28);
            this.menuStrip1.TabIndex = 12;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // informatiiToolStripMenuItem
            // 
            this.informatiiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.numarLotToolStripMenuItem});
            this.informatiiToolStripMenuItem.Name = "informatiiToolStripMenuItem";
            this.informatiiToolStripMenuItem.Size = new System.Drawing.Size(86, 24);
            this.informatiiToolStripMenuItem.Text = "Informatii";
            // 
            // numarLotToolStripMenuItem
            // 
            this.numarLotToolStripMenuItem.Name = "numarLotToolStripMenuItem";
            this.numarLotToolStripMenuItem.Size = new System.Drawing.Size(154, 26);
            this.numarLotToolStripMenuItem.Text = "Numar Lot";
            this.numarLotToolStripMenuItem.Click += new System.EventHandler(this.numarLotToolStripMenuItem_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(495, 31);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(235, 135);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 16;
            this.pictureBox2.TabStop = false;
            // 
            // tbDen
            // 
            this.tbDen.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbDen.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbDen.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbDen.HintForeColor = System.Drawing.Color.Empty;
            this.tbDen.HintText = "";
            this.tbDen.isPassword = false;
            this.tbDen.LineFocusedColor = System.Drawing.Color.BurlyWood;
            this.tbDen.LineIdleColor = System.Drawing.Color.BurlyWood;
            this.tbDen.LineMouseHoverColor = System.Drawing.Color.BurlyWood;
            this.tbDen.LineThickness = 3;
            this.tbDen.Location = new System.Drawing.Point(179, 57);
            this.tbDen.Margin = new System.Windows.Forms.Padding(4);
            this.tbDen.Name = "tbDen";
            this.tbDen.Size = new System.Drawing.Size(245, 39);
            this.tbDen.TabIndex = 17;
            this.tbDen.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // tbNr
            // 
            this.tbNr.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbNr.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbNr.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbNr.HintForeColor = System.Drawing.Color.Empty;
            this.tbNr.HintText = "";
            this.tbNr.isPassword = false;
            this.tbNr.LineFocusedColor = System.Drawing.Color.BurlyWood;
            this.tbNr.LineIdleColor = System.Drawing.Color.BurlyWood;
            this.tbNr.LineMouseHoverColor = System.Drawing.Color.BurlyWood;
            this.tbNr.LineThickness = 3;
            this.tbNr.Location = new System.Drawing.Point(179, 137);
            this.tbNr.Margin = new System.Windows.Forms.Padding(4);
            this.tbNr.Name = "tbNr";
            this.tbNr.Size = new System.Drawing.Size(245, 39);
            this.tbNr.TabIndex = 18;
            this.tbNr.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbNr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNr_KeyPress_1);
            // 
            // bunifuThinButton21
            // 
            this.bunifuThinButton21.ActiveBorderThickness = 1;
            this.bunifuThinButton21.ActiveCornerRadius = 20;
            this.bunifuThinButton21.ActiveFillColor = System.Drawing.Color.BurlyWood;
            this.bunifuThinButton21.ActiveForecolor = System.Drawing.Color.White;
            this.bunifuThinButton21.ActiveLineColor = System.Drawing.Color.BurlyWood;
            this.bunifuThinButton21.BackColor = System.Drawing.Color.White;
            this.bunifuThinButton21.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuThinButton21.BackgroundImage")));
            this.bunifuThinButton21.ButtonText = "Inregistreaza lot";
            this.bunifuThinButton21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuThinButton21.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuThinButton21.ForeColor = System.Drawing.Color.BurlyWood;
            this.bunifuThinButton21.IdleBorderThickness = 1;
            this.bunifuThinButton21.IdleCornerRadius = 20;
            this.bunifuThinButton21.IdleFillColor = System.Drawing.Color.White;
            this.bunifuThinButton21.IdleForecolor = System.Drawing.Color.BurlyWood;
            this.bunifuThinButton21.IdleLineColor = System.Drawing.Color.BurlyWood;
            this.bunifuThinButton21.Location = new System.Drawing.Point(432, 315);
            this.bunifuThinButton21.Margin = new System.Windows.Forms.Padding(5);
            this.bunifuThinButton21.Name = "bunifuThinButton21";
            this.bunifuThinButton21.Size = new System.Drawing.Size(197, 55);
            this.bunifuThinButton21.TabIndex = 19;
            this.bunifuThinButton21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuThinButton21.Click += new System.EventHandler(this.bunifuThinButton21_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(179, 239);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(245, 23);
            this.dateTimePicker1.TabIndex = 20;
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(730, 450);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.bunifuThinButton21);
            this.Controls.Add(this.tbNr);
            this.Controls.Add(this.tbDen);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form4";
            this.Text = "Form4";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem informatiiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem numarLotToolStripMenuItem;
        private System.Windows.Forms.RadioButton radioButtonFranta;
        private System.Windows.Forms.RadioButton radioButtonRomania;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Bunifu.Framework.UI.BunifuThinButton2 bunifuThinButton21;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbNr;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbDen;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}