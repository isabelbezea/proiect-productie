﻿namespace PROIECTpaw
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.bunifuThinButton21 = new Bunifu.Framework.UI.BunifuThinButton2();
            this.tbDenumire = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.tbNr = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.tbData = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.tbInfo = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.tbKcal = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.graficInformatiiNutritionaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pieChartValoriEnergeticeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.listViewProduseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.incarcaDateDinBazaDeDateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 57);
            this.label2.TabIndex = 9;
            this.label2.Text = "Denumire\r\n\r\n\r\n";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(336, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 19);
            this.label4.TabIndex = 11;
            this.label4.Text = "Data expirare";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(14, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 19);
            this.label5.TabIndex = 12;
            this.label5.Text = "Nr bucati";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(336, 129);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(197, 38);
            this.label7.TabIndex = 14;
            this.label7.Text = "Informatii nutritionale /100g\r\n(proteine, lipide, glucide)\r\n";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(893, 219);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(186, 19);
            this.label3.TabIndex = 20;
            this.label3.Text = "Valoare energetica (kcal)";
            // 
            // bunifuThinButton21
            // 
            this.bunifuThinButton21.ActiveBorderThickness = 1;
            this.bunifuThinButton21.ActiveCornerRadius = 20;
            this.bunifuThinButton21.ActiveFillColor = System.Drawing.Color.LightCoral;
            this.bunifuThinButton21.ActiveForecolor = System.Drawing.Color.White;
            this.bunifuThinButton21.ActiveLineColor = System.Drawing.Color.LightCoral;
            this.bunifuThinButton21.BackColor = System.Drawing.Color.White;
            this.bunifuThinButton21.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuThinButton21.BackgroundImage")));
            this.bunifuThinButton21.ButtonText = "Adauga produs";
            this.bunifuThinButton21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuThinButton21.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuThinButton21.ForeColor = System.Drawing.Color.LightCoral;
            this.bunifuThinButton21.IdleBorderThickness = 1;
            this.bunifuThinButton21.IdleCornerRadius = 20;
            this.bunifuThinButton21.IdleFillColor = System.Drawing.Color.White;
            this.bunifuThinButton21.IdleForecolor = System.Drawing.Color.LightCoral;
            this.bunifuThinButton21.IdleLineColor = System.Drawing.Color.LightCoral;
            this.bunifuThinButton21.Location = new System.Drawing.Point(629, 351);
            this.bunifuThinButton21.Margin = new System.Windows.Forms.Padding(5);
            this.bunifuThinButton21.Name = "bunifuThinButton21";
            this.bunifuThinButton21.Size = new System.Drawing.Size(178, 55);
            this.bunifuThinButton21.TabIndex = 21;
            this.bunifuThinButton21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuThinButton21.Click += new System.EventHandler(this.bunifuThinButton21_Click);
            // 
            // tbDenumire
            // 
            this.tbDenumire.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbDenumire.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbDenumire.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbDenumire.HintForeColor = System.Drawing.Color.Empty;
            this.tbDenumire.HintText = "";
            this.tbDenumire.isPassword = false;
            this.tbDenumire.LineFocusedColor = System.Drawing.Color.LightCoral;
            this.tbDenumire.LineIdleColor = System.Drawing.Color.LightCoral;
            this.tbDenumire.LineMouseHoverColor = System.Drawing.Color.LightCoral;
            this.tbDenumire.LineThickness = 3;
            this.tbDenumire.Location = new System.Drawing.Point(13, 81);
            this.tbDenumire.Margin = new System.Windows.Forms.Padding(4);
            this.tbDenumire.Name = "tbDenumire";
            this.tbDenumire.Size = new System.Drawing.Size(161, 44);
            this.tbDenumire.TabIndex = 24;
            this.tbDenumire.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // tbNr
            // 
            this.tbNr.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbNr.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbNr.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbNr.HintForeColor = System.Drawing.Color.Empty;
            this.tbNr.HintText = "";
            this.tbNr.isPassword = false;
            this.tbNr.LineFocusedColor = System.Drawing.Color.LightCoral;
            this.tbNr.LineIdleColor = System.Drawing.Color.LightCoral;
            this.tbNr.LineMouseHoverColor = System.Drawing.Color.LightCoral;
            this.tbNr.LineThickness = 3;
            this.tbNr.Location = new System.Drawing.Point(15, 171);
            this.tbNr.Margin = new System.Windows.Forms.Padding(4);
            this.tbNr.Name = "tbNr";
            this.tbNr.Size = new System.Drawing.Size(161, 44);
            this.tbNr.TabIndex = 25;
            this.tbNr.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbNr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNr_KeyPress_1);
            // 
            // tbData
            // 
            this.tbData.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbData.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbData.HintForeColor = System.Drawing.Color.Empty;
            this.tbData.HintText = "";
            this.tbData.isPassword = false;
            this.tbData.LineFocusedColor = System.Drawing.Color.LightCoral;
            this.tbData.LineIdleColor = System.Drawing.Color.LightCoral;
            this.tbData.LineMouseHoverColor = System.Drawing.Color.LightCoral;
            this.tbData.LineThickness = 3;
            this.tbData.Location = new System.Drawing.Point(340, 81);
            this.tbData.Margin = new System.Windows.Forms.Padding(4);
            this.tbData.Name = "tbData";
            this.tbData.Size = new System.Drawing.Size(161, 44);
            this.tbData.TabIndex = 26;
            this.tbData.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // tbInfo
            // 
            this.tbInfo.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbInfo.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbInfo.HintForeColor = System.Drawing.Color.Empty;
            this.tbInfo.HintText = "";
            this.tbInfo.isPassword = false;
            this.tbInfo.LineFocusedColor = System.Drawing.Color.LightCoral;
            this.tbInfo.LineIdleColor = System.Drawing.Color.LightCoral;
            this.tbInfo.LineMouseHoverColor = System.Drawing.Color.LightCoral;
            this.tbInfo.LineThickness = 3;
            this.tbInfo.Location = new System.Drawing.Point(340, 171);
            this.tbInfo.Margin = new System.Windows.Forms.Padding(4);
            this.tbInfo.Name = "tbInfo";
            this.tbInfo.Size = new System.Drawing.Size(161, 44);
            this.tbInfo.TabIndex = 28;
            this.tbInfo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbInfo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbInfo_KeyPress_1);
            // 
            // tbKcal
            // 
            this.tbKcal.BorderColorFocused = System.Drawing.Color.LightCoral;
            this.tbKcal.BorderColorIdle = System.Drawing.Color.LightCoral;
            this.tbKcal.BorderColorMouseHover = System.Drawing.Color.LightCoral;
            this.tbKcal.BorderThickness = 3;
            this.tbKcal.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbKcal.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbKcal.ForeColor = System.Drawing.Color.LightCoral;
            this.tbKcal.isPassword = false;
            this.tbKcal.Location = new System.Drawing.Point(913, 261);
            this.tbKcal.Margin = new System.Windows.Forms.Padding(4);
            this.tbKcal.Name = "tbKcal";
            this.tbKcal.Size = new System.Drawing.Size(142, 41);
            this.tbKcal.TabIndex = 29;
            this.tbKcal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1000, -14);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(235, 135);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 30;
            this.pictureBox1.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.graficInformatiiNutritionaleToolStripMenuItem,
            this.pieChartValoriEnergeticeToolStripMenuItem,
            this.incarcaDateDinBazaDeDateToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1180, 28);
            this.menuStrip1.TabIndex = 32;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // graficInformatiiNutritionaleToolStripMenuItem
            // 
            this.graficInformatiiNutritionaleToolStripMenuItem.Name = "graficInformatiiNutritionaleToolStripMenuItem";
            this.graficInformatiiNutritionaleToolStripMenuItem.Size = new System.Drawing.Size(209, 24);
            this.graficInformatiiNutritionaleToolStripMenuItem.Text = "Grafic informatii nutritionale";
            this.graficInformatiiNutritionaleToolStripMenuItem.Click += new System.EventHandler(this.graficInformatiiNutritionaleToolStripMenuItem_Click);
            // 
            // pieChartValoriEnergeticeToolStripMenuItem
            // 
            this.pieChartValoriEnergeticeToolStripMenuItem.Name = "pieChartValoriEnergeticeToolStripMenuItem";
            this.pieChartValoriEnergeticeToolStripMenuItem.Size = new System.Drawing.Size(196, 24);
            this.pieChartValoriEnergeticeToolStripMenuItem.Text = "Pie Chart Valori Energetice";
            this.pieChartValoriEnergeticeToolStripMenuItem.Click += new System.EventHandler(this.pieChartValoriEnergeticeToolStripMenuItem_Click);
            // 
            // listBox1
            // 
            this.listBox1.AllowDrop = true;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(17, 351);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(440, 52);
            this.listBox1.TabIndex = 33;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            this.listBox1.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox1_DragDrop);
            this.listBox1.DragEnter += new System.Windows.Forms.DragEventHandler(this.listBox1_DragEnter);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(539, 58);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(348, 277);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 34;
            this.pictureBox2.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(14, 322);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(446, 19);
            this.label6.TabIndex = 36;
            this.label6.Text = "Drag and Drop imagine cu produsul pe care vrei sa il inregistrezi";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listViewProduseToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(191, 28);
            // 
            // listViewProduseToolStripMenuItem
            // 
            this.listViewProduseToolStripMenuItem.Name = "listViewProduseToolStripMenuItem";
            this.listViewProduseToolStripMenuItem.Size = new System.Drawing.Size(190, 24);
            this.listViewProduseToolStripMenuItem.Text = "ListView produse";
            // 
            // incarcaDateDinBazaDeDateToolStripMenuItem
            // 
            this.incarcaDateDinBazaDeDateToolStripMenuItem.Name = "incarcaDateDinBazaDeDateToolStripMenuItem";
            this.incarcaDateDinBazaDeDateToolStripMenuItem.Size = new System.Drawing.Size(218, 24);
            this.incarcaDateDinBazaDeDateToolStripMenuItem.Text = "Incarca date din baza de date";
            this.incarcaDateDinBazaDeDateToolStripMenuItem.Click += new System.EventHandler(this.incarcaDateDinBazaDeDateToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1180, 450);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.tbKcal);
            this.Controls.Add(this.tbInfo);
            this.Controls.Add(this.tbData);
            this.Controls.Add(this.tbNr);
            this.Controls.Add(this.tbDenumire);
            this.Controls.Add(this.bunifuThinButton21);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label label3;
        private Bunifu.Framework.UI.BunifuThinButton2 bunifuThinButton21;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbInfo;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbData;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbNr;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbDenumire;
        private Bunifu.Framework.UI.BunifuMetroTextbox tbKcal;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem graficInformatiiNutritionaleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pieChartValoriEnergeticeToolStripMenuItem;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem listViewProduseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem incarcaDateDinBazaDeDateToolStripMenuItem;
    }
}