﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROIECTpaw
{

    public class Produs :ICloneable, IComparable, IValoareEnergetica
    {
        private int idProdus;
        private string denumireProdus;
        private int nrBucati;
        private DateTime dataExpirare;
        private float[] informatiiNutritionale;
        private MateriePrima[] materiiPrime;
        private float pretProductie;
        private float pretVanzare;
        string path;





        public Produs(int idProdus, string denumireProdus, int nrBucati, DateTime dataExpirare, float[] informatiiNutritionale, float pretProductie, float pretVanzare, string path)
        {
            this.idProdus = idProdus;
            this.denumireProdus = denumireProdus;
            this.nrBucati = nrBucati;
            this.dataExpirare = dataExpirare;
            this.informatiiNutritionale = new float[informatiiNutritionale.Length];
            for (int i = 0; i < informatiiNutritionale.Length; i++)
                this.informatiiNutritionale[i] = informatiiNutritionale[i];
            this.pretProductie = pretProductie;
            this.pretVanzare = pretVanzare;
            this.path=path;


        }
        public Produs(int idProdus, string denumireProdus, int nrBucati, DateTime dataExpirare, float[] informatiiNutritionale)
        {
            this.idProdus = idProdus;
            this.denumireProdus = denumireProdus;
            this.nrBucati = nrBucati;
            this.dataExpirare = dataExpirare;
            this.informatiiNutritionale = new float[informatiiNutritionale.Length];
            for (int i = 0; i < informatiiNutritionale.Length; i++)
                this.informatiiNutritionale[i] = informatiiNutritionale[i];
          

        }
        public Produs(int idProdus, string denumireProdus, int nrBucati, DateTime dataExpirare, float[] informatiiNutritionale, string path)
        {
            this.idProdus = idProdus;
            this.denumireProdus = denumireProdus;
            this.nrBucati = nrBucati;
            this.dataExpirare = dataExpirare;
            this.informatiiNutritionale = new float[informatiiNutritionale.Length];
            for (int i = 0; i < informatiiNutritionale.Length; i++)
                this.informatiiNutritionale[i] = informatiiNutritionale[i];
            this.path = path;


        }


        public int IdProdus
        {
            get
            {
                return this.idProdus;
            }

            set
            {
                this.idProdus = value;
            }
        }

        public string DenumireProdus
        {
            get
            {
                return this.denumireProdus;
            }

            set
            {
                this.denumireProdus = value;
            }
        }
        public int NrBuc
        {
            get
            {
                return this.nrBucati;
            }

            set
            {
                this.nrBucati = value;
            }
        }
       
        public DateTime DataExpirare
        {
            get
            {
                return this.dataExpirare;
            }

            set
            {
                this.dataExpirare = value;
            }
        }

        public float[] InformatiiNutritionale
        {
            get
            {
                return this.informatiiNutritionale;
            }

            set
            {
                this.informatiiNutritionale = value;
            }
        }
        public string getDenumireDinObiect(int i)
        {
            return this.materiiPrime[i].DenumireMateriePrima;

        }
        public float getCantitateDinObiect(int i)
        {
            return this.materiiPrime[i].CantitateMateriePrima;
        }

        public MateriePrima[] MateriiPrime
        {
            get
            {
                return this.materiiPrime;
            }

            set
            {
                this.materiiPrime = value;
            }
        }
        public float PretProductie
        {
            get
            {
                return this.pretProductie;
            }

            set
            {
                this.pretProductie = value;
            }
        }
        public float PretVanzare
        {
            get
            {
                return this.pretVanzare;
            }

            set
            {
                this.pretVanzare = value;
            }
        }
        public string Path
        {
            get
            {
                return this.path;
            }

            set
            {
                this.path = value;
            }
        }
        
        public object Clone()
        {
            Produs p = (Produs)this.MemberwiseClone();

            float[] copieValoareNutritionala = (float[])this.informatiiNutritionale.Clone();

            p.informatiiNutritionale = copieValoareNutritionala;

            return p;
        }

        public int CompareTo(object obj)
        {
            Produs p = (Produs)obj;

            if (this.pretVanzare < p.pretVanzare)
            {
                return -1;
            }
            else if (this.pretVanzare > p.pretVanzare)
            {
                return 1;
            }
            else
            {
                return string.Compare(this.denumireProdus, p.denumireProdus);
            }
        }

        public double CalculeazaValoareEnergetica()
        {
            return (double)this;
        }

        public float this[int index]
        {
            get
            {
                if (informatiiNutritionale != null && index >= 0 && index < informatiiNutritionale.Length)
                    return informatiiNutritionale[index];
                else
                    return 0;
            }
            set
            {
                if (value > 0 && value <= 1000 &&
                    index >= 0 && index < informatiiNutritionale.Length)
                    informatiiNutritionale[index] = value;
            }
        }

        public static explicit operator double(Produs p)
        {
            double ve=0;
            if (p.informatiiNutritionale != null)
            {
                ve = p.informatiiNutritionale[0] * 4.1 + p.informatiiNutritionale[1] * 9.3 + p.informatiiNutritionale[2] * 4.1;

                return ve;
            }
            else return 0;
           }
        public float Valoare()
        {
            float val = 0f;
            val = pretVanzare * nrBucati;
            return val;
        }
       
        public void Majorare( float procent)
        {
            if(informatiiNutritionale!=null)
                for(int i=0;i<informatiiNutritionale.Length;i++)
                
                    informatiiNutritionale[i] = (1 + procent) * informatiiNutritionale[i];

        }
        

        public override string ToString()
        {
            string result = null;
            {

                result += "Produsul cu id-ul " + this.idProdus + " este " + this.denumireProdus + Environment.NewLine
                  + "Produsul expira la data de " + this.dataExpirare + Environment.NewLine
                  + "Numar bucati disponibile: " + this.nrBucati + Environment.NewLine; 
                

                if (this.materiiPrime != null)

                {
                    result += Environment.NewLine+"MATERII PRIME NECESARE: "+Environment.NewLine;
                    for (int i = 0; i < this.MateriiPrime.Length; i++)
                    {
                        result += materiiPrime[i].DenumireMateriePrima + " in cantitatea de " + materiiPrime[i].CantitateMateriePrima + " grame; ";

                    }

                }

                if (this.informatiiNutritionale.Length != 0)
                {
                    result += Environment.NewLine+Environment.NewLine+"INFORMATII NUTRITIONALE: "+Environment.NewLine + this.informatiiNutritionale[0] +" grame proteine, " + this.informatiiNutritionale[1] + " grame lipide, " + this.informatiiNutritionale[2] + " grame glucide. Valoare energetica (kcal): "+this.informatiiNutritionale[3]+" kcal.";


                }
                result += Environment.NewLine + Environment.NewLine;
                if (this.pretProductie != 0 && this.pretVanzare != 0)
                    result += Environment.NewLine + "Pret productie: " + this.pretProductie + " lei. Pret vanzare: " + this.pretVanzare + " lei.";
                else
                    result += "Momentan pretul de productie si pretul de vanzare pentru acest produs nu sunt disponibile intrucat nu au fost introduse materiile prime necesare prepararii";

                result += Environment.NewLine + Environment.NewLine + Environment.NewLine;

                return result;

            }

        }
    }
}
