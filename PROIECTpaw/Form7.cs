﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Data.OleDb;
namespace PROIECTpaw
{
    public partial class Form7 : Form
    {
        string connString;
        public Form7()
        {
            InitializeComponent();
            connString = "Provider = Microsoft.ACE.OLEDB.12.0; Data Source = produse.accdb";
           
        }

        private void Form7_Load(object sender, EventArgs e)
        {
            Chart pieChart = new Chart();
            pieChart.Size = this.Size - new Size(0, 40);//center it in form

            ChartArea area = new ChartArea("PieChartArea");
            area.BorderWidth = this.Width;
            pieChart.ChartAreas.Add(area);
            pieChart.Series.Clear();
            pieChart.Palette = ChartColorPalette.Chocolate;
            pieChart.BackColor = Color.PeachPuff;
            pieChart.Titles.Add("Valori energetice (kcal)"); 
            pieChart.ChartAreas[0].BackColor = Color.Transparent;

            Legend l = new Legend()
            {
                BackColor = Color.Transparent,
                ForeColor = Color.Black,
                Title = "Legenda"
            };

            pieChart.Legends.Add(l);

            Series s1 = new Series()
            {
                Name = "s1",
                IsVisibleInLegend = true,
                Color = Color.Transparent,
                ChartType = SeriesChartType.Pie
            };

            pieChart.Series.Add(s1);


            OleDbConnection conexiune = new OleDbConnection(connString);
            try
            {

                conexiune.Open();
                OleDbCommand comanda = new OleDbCommand();
                comanda.CommandText = "SELECT * FROM produs";
                comanda.Connection = conexiune;
                OleDbDataReader reader = comanda.ExecuteReader();
                while (reader.Read())
                {

                    DataPoint d = new DataPoint(0, Convert.ToDouble(reader["Kcal"]));
                    d.AxisLabel = reader["Kcal"].ToString();
                    d.LegendText = reader["Denumire"].ToString();

                    s1.Points.Add(d);

                }
           
                


           

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
            finally
            {
                conexiune.Close();

            }
            this.Controls.Add(pieChart); 



        }
    }
}
