﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.IO;

namespace PROIECTpaw
{
    public partial class Form1 :Form, IValoareEnergetica

    {

       // public List<Produs> listaProd = new List<Produs>();
        public Form1()
        {
            InitializeComponent();
          


        }



      
        public float[] GetVector()
        {
            string[] SVector = tbInfo.Text.Split(',');
            float[] informatiiNutritionale = new float[SVector.Length + 1];
            for (int i = 0; i < SVector.Length; i++)
                informatiiNutritionale[i] = float.Parse(SVector[i]);
            return informatiiNutritionale;
        }

         public double CalculeazaValoareEnergetica()
        {
            
            float[] informatiiNutritionale=GetVector();


            double ve = 0;
            if (informatiiNutritionale != null)

                ve = informatiiNutritionale[0] * 4.1 + informatiiNutritionale[1] * 9.3 + informatiiNutritionale[2] * 4.1;

            return ve;
        }

       

        private void bunifuThinButton21_Click(object sender, EventArgs e)
        {
            Form5 frm = new Form5();



            
            
                 if (tbDenumire.Text == "")
                errorProvider1.SetError(tbDenumire, "Denumirea trebuie introdusa!");
            else
                 if (tbNr.Text == "")
                errorProvider1.SetError(tbNr, "Numarul de bucati trebuie introdus!");
            else
                 if (tbData.Text == "")
                errorProvider1.SetError(tbData, "Data trebuie introdusa!");

            else
                 if (tbInfo.Text == "")
                errorProvider1.SetError(tbInfo, "Informatiile nutritionale trebuie introduse!");
            else
                if (pictureBox2.Image == null)
                errorProvider1.SetError(pictureBox2, "Trebuie sa introduceti o imagine pentru produsul respectiv!");
            OleDbConnection conexiune = new OleDbConnection("Provider = Microsoft.ACE.OLEDB.12.0; Data Source = produse.accdb");
            try
            {
               
                conexiune.Open();
                OleDbCommand comanda = new OleDbCommand();
                comanda.Connection = conexiune;

                comanda.CommandText = " SELECT MAX(ID) FROM produs";
                int id = Convert.ToInt32(comanda.ExecuteScalar()); 
                comanda.CommandText="INSERT INTO produs VALUES(?,?,?,?,?,?,?,?,?,?,?)";
                comanda.Parameters.Add("ID", OleDbType.Integer).Value = id + 1;
                comanda.Parameters.Add("Denumire", OleDbType.Char, 20).Value = tbDenumire.Text;
                comanda.Parameters.Add("NumarBucati", OleDbType.Integer).Value = Convert.ToInt32(tbNr.Text);
                comanda.Parameters.Add("DataExpirare", OleDbType.DBDate).Value = Convert.ToDateTime(tbData.Text);


                
                string pth= listBox1.SelectedItem.ToString();
                string [] components=pth.Split('\\');

                
                 string fileName=components[components.Length-1];
                comanda.Parameters.Add("PATH", OleDbType.Char, 255).Value = "../../../pictures_with_food/" + fileName;
                    
                comanda.Parameters.Add("Proteine", OleDbType.Double).Value = Convert.ToDouble(tbInfo.Text.Split(',')[0]);
                comanda.Parameters.Add("Lipide", OleDbType.Double).Value = Convert.ToDouble(tbInfo.Text.Split(',')[1]);
                comanda.Parameters.Add("Glucide", OleDbType.Double).Value = Convert.ToDouble(tbInfo.Text.Split(',')[2]);
                
                float[] informatiiNutritionale = GetVector();
                double ve; 
                ve = CalculeazaValoareEnergetica();


                informatiiNutritionale[3] = (float)ve;

                tbKcal.Text = ve.ToString();

                comanda.Parameters.Add("Kcal", OleDbType.Double).Value = Convert.ToDouble(informatiiNutritionale[3]);
                comanda.Parameters.Add("Pret fabricatie", OleDbType.Double).Value = 0;
                comanda.Parameters.Add("Pret vanzare", OleDbType.Double).Value = 0;

                comanda.ExecuteNonQuery();
                MessageBox.Show("Produsul a fost adaugat cu succes in baza de date!");
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
            finally
            {
                
                tbDenumire.Text = "";
                tbData.Text="";
                tbNr.Text = "";
               
                tbInfo.Text="";
                tbKcal.Text="";
                pictureBox2.Image = null;
                
                errorProvider1.Clear();
                conexiune.Close(); 
            }
        }

        
        private void tbPret_Validating_1(object sender, CancelEventArgs e)
        {
            try
            {
               
            }
            catch (FormatException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

       

        private void tbPret_KeyPress_2(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void tbNr_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void tbInfo_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) &&
             !char.IsControl(e.KeyChar) && e.KeyChar != ',' && e.KeyChar != '.')
                e.Handled = true;
        }

        private void graficInformatiiNutritionaleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form6 obj = new Form6();
            obj.ShowDialog();

        }

        private void pieChartValoriEnergeticeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form7 obj = new Form7();
            obj.ShowDialog();
        }

       

        private void listBox1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        private void listBox1_DragDrop(object sender, DragEventArgs e)
        {
            string[] fileList = (string[])e.Data.GetData(DataFormats.FileDrop);

            foreach (string s in fileList)
                listBox1.Items.Add(s);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string pathSource = listBox1.SelectedItem.ToString();

            //string fileToCopy = "c:\\myFolder\\myFile.txt";
            string destinationDirectory = "../../../pictures_with_food/";

            File.Copy(pathSource, destinationDirectory + Path.GetFileName(pathSource));
            pictureBox2.ImageLocation = pathSource;
            
        }

       

       
        private void incarcaDateDinBazaDeDateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form8 frm = new Form8();
            frm.Show();
        }
    }

}
