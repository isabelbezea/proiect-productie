﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace PROIECTpaw
{
    public partial class Form8 : Form
    {
        string connString;
        public Form8()
        {
            InitializeComponent();
            connString = "Provider = Microsoft.ACE.OLEDB.12.0; Data Source = produse.accdb";

        }

        private void bunifuThinButton21_Click(object sender, EventArgs e)
        {
            OleDbConnection conexiune = new OleDbConnection(connString);
            try
            {
                conexiune.Open();
                // MessageBox.Show("a mers");
                OleDbCommand comanda = new OleDbCommand();
                comanda.CommandText = " SELECT * FROM produs";
                comanda.Connection = conexiune;

                OleDbDataReader reader = comanda.ExecuteReader();
                while(reader.Read())
                {
                    ListViewItem itm = new ListViewItem(reader["ID"].ToString());
                    itm.SubItems.Add(reader["Denumire"].ToString());
                    itm.SubItems.Add(reader["NumarBucati"].ToString());
                    itm.SubItems.Add(reader["DataExpirare"].ToString());
                    itm.SubItems.Add(reader["PATH"].ToString());
                    itm.SubItems.Add(reader["Proteine"].ToString());
                    itm.SubItems.Add(reader["Lipide"].ToString());
                    itm.SubItems.Add(reader["Glucide"].ToString());
                    itm.SubItems.Add(reader["Kcal"].ToString());
                    itm.SubItems.Add(reader["Pret_fabricatie"].ToString());
                    itm.SubItems.Add(reader["Pret_vanzare"].ToString());
                    listView1.Items.Add(itm);

                }

            }
            catch (Exception ex)
             {
                MessageBox.Show(ex.Message);
               }
            finally
            {
                conexiune.Close();

            }
        }

        private void Form8_Load(object sender, EventArgs e)
        {

        }
    }
}
