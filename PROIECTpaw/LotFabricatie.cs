﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROIECTpaw
{
    class LotFabricatie
    { string denumireProdus;
        string taraProvenienta;
        int nrLinieProductie;
        int anProductie;
        int ziuaProductie; //numarul succesiv din an


        public LotFabricatie(string denumireProdus, string taraProvenienta, int nrLinieProductie, int anProductie, int ziuaProductie)
        {
            this.denumireProdus = denumireProdus; 
            this.taraProvenienta = taraProvenienta;
            this.nrLinieProductie = nrLinieProductie;
            this.anProductie = anProductie;
            this.ziuaProductie = ziuaProductie;
        }
        public string DenumireProdus
        {
            get
            {
                return this.denumireProdus;
            }
            set
            {
                this.denumireProdus = value;
            }
        }
        public string TaraProvenienta
        {
            get
            {
                return this.taraProvenienta;
            }
            set
            {
                this.taraProvenienta = value;
            }
        }
        public int NrLinieProductie
        {
            get
            {
                return this.nrLinieProductie;
            }
            set
            {
                this.nrLinieProductie = value;
            }
        }
        public int AnProductie
        {
            get
            {
                return this.anProductie;
            }
            set
            {
                this.anProductie = value;
            }
        }
        public int ZiuaProductie
        {
            get
            {
                return this.ziuaProductie;
            }
            set
            {
                this.ziuaProductie = value;
            }
        }
        public override string ToString()
        {
            string result = null;
            result += "Lotul ce contine " + this.denumireProdus + " a fost adaugat! " + Environment.NewLine;
                

            return result; 
        }
    }
}
