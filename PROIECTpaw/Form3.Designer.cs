﻿namespace PROIECTpaw
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form3));
            this.label2 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.saveFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tbDen = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbCant = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.tbMat = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.tbNr = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnAdauga = new Bunifu.Framework.UI.BunifuThinButton2();
            this.bunifuThinButton21 = new Bunifu.Framework.UI.BunifuThinButton2();
            this.tbPret = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label1 = new System.Windows.Forms.Label();
            this.bunifuThinButton22 = new Bunifu.Framework.UI.BunifuThinButton2();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 19);
            this.label2.TabIndex = 5;
            this.label2.Text = "Denumire produs";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBox1.Location = new System.Drawing.Point(0, 334);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(800, 226);
            this.textBox1.TabIndex = 11;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveFileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 28);
            this.menuStrip1.TabIndex = 13;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // saveFileToolStripMenuItem
            // 
            this.saveFileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveFileToolStripMenuItem1});
            this.saveFileToolStripMenuItem.Name = "saveFileToolStripMenuItem";
            this.saveFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveFileToolStripMenuItem.Size = new System.Drawing.Size(70, 24);
            this.saveFileToolStripMenuItem.Text = "Optiuni";
            // 
            // saveFileToolStripMenuItem1
            // 
            this.saveFileToolStripMenuItem1.Name = "saveFileToolStripMenuItem1";
            this.saveFileToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveFileToolStripMenuItem1.Size = new System.Drawing.Size(192, 26);
            this.saveFileToolStripMenuItem1.Text = "&Save File";
            this.saveFileToolStripMenuItem1.Click += new System.EventHandler(this.saveFileToolStripMenuItem1_Click);
            // 
            // tbDen
            // 
            this.tbDen.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbDen.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbDen.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbDen.HintForeColor = System.Drawing.Color.Empty;
            this.tbDen.HintText = "";
            this.tbDen.isPassword = false;
            this.tbDen.LineFocusedColor = System.Drawing.Color.Gray;
            this.tbDen.LineIdleColor = System.Drawing.Color.Gray;
            this.tbDen.LineMouseHoverColor = System.Drawing.Color.Gray;
            this.tbDen.LineThickness = 3;
            this.tbDen.Location = new System.Drawing.Point(230, 20);
            this.tbDen.Margin = new System.Windows.Forms.Padding(4);
            this.tbDen.Name = "tbDen";
            this.tbDen.Size = new System.Drawing.Size(161, 44);
            this.tbDen.TabIndex = 15;
            this.tbDen.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 248);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(180, 19);
            this.label4.TabIndex = 7;
            this.label4.Text = "Cantitati folosite (grame)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 149);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 19);
            this.label3.TabIndex = 6;
            this.label3.Text = "Materii prime";
            // 
            // tbCant
            // 
            this.tbCant.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbCant.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbCant.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbCant.HintForeColor = System.Drawing.Color.Empty;
            this.tbCant.HintText = "";
            this.tbCant.isPassword = false;
            this.tbCant.LineFocusedColor = System.Drawing.Color.Gray;
            this.tbCant.LineIdleColor = System.Drawing.Color.Gray;
            this.tbCant.LineMouseHoverColor = System.Drawing.Color.Gray;
            this.tbCant.LineThickness = 3;
            this.tbCant.Location = new System.Drawing.Point(230, 223);
            this.tbCant.Margin = new System.Windows.Forms.Padding(4);
            this.tbCant.Name = "tbCant";
            this.tbCant.Size = new System.Drawing.Size(161, 44);
            this.tbCant.TabIndex = 18;
            this.tbCant.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbCant.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbCant_KeyPress_1);
            // 
            // tbMat
            // 
            this.tbMat.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbMat.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbMat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbMat.HintForeColor = System.Drawing.Color.Empty;
            this.tbMat.HintText = "";
            this.tbMat.isPassword = false;
            this.tbMat.LineFocusedColor = System.Drawing.Color.Gray;
            this.tbMat.LineIdleColor = System.Drawing.Color.Gray;
            this.tbMat.LineMouseHoverColor = System.Drawing.Color.Gray;
            this.tbMat.LineThickness = 3;
            this.tbMat.Location = new System.Drawing.Point(230, 124);
            this.tbMat.Margin = new System.Windows.Forms.Padding(4);
            this.tbMat.Name = "tbMat";
            this.tbMat.Size = new System.Drawing.Size(161, 44);
            this.tbMat.TabIndex = 17;
            this.tbMat.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // tbNr
            // 
            this.tbNr.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbNr.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbNr.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbNr.HintForeColor = System.Drawing.Color.Empty;
            this.tbNr.HintText = "";
            this.tbNr.isPassword = false;
            this.tbNr.LineFocusedColor = System.Drawing.Color.Gray;
            this.tbNr.LineIdleColor = System.Drawing.Color.Gray;
            this.tbNr.LineMouseHoverColor = System.Drawing.Color.Gray;
            this.tbNr.LineThickness = 3;
            this.tbNr.Location = new System.Drawing.Point(230, 72);
            this.tbNr.Margin = new System.Windows.Forms.Padding(4);
            this.tbNr.Name = "tbNr";
            this.tbNr.Size = new System.Drawing.Size(161, 44);
            this.tbNr.TabIndex = 16;
            this.tbNr.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbNr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNr_KeyPress_2);
            this.tbNr.Validating += new System.ComponentModel.CancelEventHandler(this.tbNr_Validating_1);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 97);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(200, 19);
            this.label5.TabIndex = 8;
            this.label5.Text = "Numar materii prime folosite";
            // 
            // btnAdauga
            // 
            this.btnAdauga.ActiveBorderThickness = 1;
            this.btnAdauga.ActiveCornerRadius = 20;
            this.btnAdauga.ActiveFillColor = System.Drawing.Color.Gray;
            this.btnAdauga.ActiveForecolor = System.Drawing.Color.White;
            this.btnAdauga.ActiveLineColor = System.Drawing.Color.Gray;
            this.btnAdauga.BackColor = System.Drawing.SystemColors.Control;
            this.btnAdauga.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAdauga.BackgroundImage")));
            this.btnAdauga.ButtonText = "Adauga ";
            this.btnAdauga.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAdauga.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdauga.ForeColor = System.Drawing.Color.Gray;
            this.btnAdauga.IdleBorderThickness = 1;
            this.btnAdauga.IdleCornerRadius = 20;
            this.btnAdauga.IdleFillColor = System.Drawing.Color.White;
            this.btnAdauga.IdleForecolor = System.Drawing.Color.Gray;
            this.btnAdauga.IdleLineColor = System.Drawing.Color.Gray;
            this.btnAdauga.Location = new System.Drawing.Point(502, 113);
            this.btnAdauga.Margin = new System.Windows.Forms.Padding(5);
            this.btnAdauga.Name = "btnAdauga";
            this.btnAdauga.Size = new System.Drawing.Size(179, 41);
            this.btnAdauga.TabIndex = 22;
            this.btnAdauga.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAdauga.Click += new System.EventHandler(this.btnAdauga_Click);
            // 
            // bunifuThinButton21
            // 
            this.bunifuThinButton21.ActiveBorderThickness = 1;
            this.bunifuThinButton21.ActiveCornerRadius = 20;
            this.bunifuThinButton21.ActiveFillColor = System.Drawing.Color.Gray;
            this.bunifuThinButton21.ActiveForecolor = System.Drawing.Color.White;
            this.bunifuThinButton21.ActiveLineColor = System.Drawing.Color.Gray;
            this.bunifuThinButton21.BackColor = System.Drawing.SystemColors.Control;
            this.bunifuThinButton21.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuThinButton21.BackgroundImage")));
            this.bunifuThinButton21.ButtonText = "Afisare lista produse actualizata";
            this.bunifuThinButton21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuThinButton21.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuThinButton21.ForeColor = System.Drawing.Color.Gray;
            this.bunifuThinButton21.IdleBorderThickness = 1;
            this.bunifuThinButton21.IdleCornerRadius = 20;
            this.bunifuThinButton21.IdleFillColor = System.Drawing.Color.White;
            this.bunifuThinButton21.IdleForecolor = System.Drawing.Color.Gray;
            this.bunifuThinButton21.IdleLineColor = System.Drawing.Color.Gray;
            this.bunifuThinButton21.Location = new System.Drawing.Point(429, 164);
            this.bunifuThinButton21.Margin = new System.Windows.Forms.Padding(5);
            this.bunifuThinButton21.Name = "bunifuThinButton21";
            this.bunifuThinButton21.Size = new System.Drawing.Size(339, 41);
            this.bunifuThinButton21.TabIndex = 23;
            this.bunifuThinButton21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuThinButton21.Click += new System.EventHandler(this.bunifuThinButton21_Click);
            // 
            // tbPret
            // 
            this.tbPret.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbPret.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbPret.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbPret.HintForeColor = System.Drawing.Color.Empty;
            this.tbPret.HintText = "";
            this.tbPret.isPassword = false;
            this.tbPret.LineFocusedColor = System.Drawing.Color.Gray;
            this.tbPret.LineIdleColor = System.Drawing.Color.Gray;
            this.tbPret.LineMouseHoverColor = System.Drawing.Color.Gray;
            this.tbPret.LineThickness = 3;
            this.tbPret.Location = new System.Drawing.Point(230, 172);
            this.tbPret.Margin = new System.Windows.Forms.Padding(4);
            this.tbPret.Name = "tbPret";
            this.tbPret.Size = new System.Drawing.Size(161, 44);
            this.tbPret.TabIndex = 24;
            this.tbPret.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbPret.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPret_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 197);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 19);
            this.label1.TabIndex = 25;
            this.label1.Text = "Lei/kg materii prime";
            // 
            // bunifuThinButton22
            // 
            this.bunifuThinButton22.ActiveBorderThickness = 1;
            this.bunifuThinButton22.ActiveCornerRadius = 20;
            this.bunifuThinButton22.ActiveFillColor = System.Drawing.Color.Gray;
            this.bunifuThinButton22.ActiveForecolor = System.Drawing.Color.White;
            this.bunifuThinButton22.ActiveLineColor = System.Drawing.Color.Gray;
            this.bunifuThinButton22.BackColor = System.Drawing.SystemColors.Control;
            this.bunifuThinButton22.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuThinButton22.BackgroundImage")));
            this.bunifuThinButton22.ButtonText = "Previzualizare lista produse ";
            this.bunifuThinButton22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuThinButton22.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuThinButton22.ForeColor = System.Drawing.Color.Gray;
            this.bunifuThinButton22.IdleBorderThickness = 1;
            this.bunifuThinButton22.IdleCornerRadius = 20;
            this.bunifuThinButton22.IdleFillColor = System.Drawing.Color.White;
            this.bunifuThinButton22.IdleForecolor = System.Drawing.Color.Gray;
            this.bunifuThinButton22.IdleLineColor = System.Drawing.Color.Gray;
            this.bunifuThinButton22.Location = new System.Drawing.Point(429, 215);
            this.bunifuThinButton22.Margin = new System.Windows.Forms.Padding(5);
            this.bunifuThinButton22.Name = "bunifuThinButton22";
            this.bunifuThinButton22.Size = new System.Drawing.Size(339, 41);
            this.bunifuThinButton22.TabIndex = 26;
            this.bunifuThinButton22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuThinButton22.Click += new System.EventHandler(this.bunifuThinButton22_Click);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // printDocument1
            // 
           // this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 560);
            this.Controls.Add(this.bunifuThinButton22);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbPret);
            this.Controls.Add(this.bunifuThinButton21);
            this.Controls.Add(this.btnAdauga);
            this.Controls.Add(this.tbCant);
            this.Controls.Add(this.tbMat);
            this.Controls.Add(this.tbNr);
            this.Controls.Add(this.tbDen);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form3";
            this.Text = "Form3";
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem saveFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveFileToolStripMenuItem1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbDen;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbCant;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbMat;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbNr;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private Bunifu.Framework.UI.BunifuThinButton2 btnAdauga;
        private Bunifu.Framework.UI.BunifuThinButton2 bunifuThinButton21;
        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbPret;
        private Bunifu.Framework.UI.BunifuThinButton2 bunifuThinButton22;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
    }
}