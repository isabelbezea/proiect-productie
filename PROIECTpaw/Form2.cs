﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Drawing.Printing;
using System.Data.OleDb;

namespace PROIECTpaw
{
    public partial class Form2 : Form
    {
        string Provider;

        public Form2()
        {
            InitializeComponent();
            Provider = "Provider = Microsoft.ACE.OLEDB.12.0; Data Source = produse.accdb";


        }


        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "(*.txt)|*.txt";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(dlg.FileName);
                textBox1.Text = sr.ReadToEnd();
                sr.Close();
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "(*.txt)|*.txt";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(dlg.FileName);
                sw.WriteLine(textBox1.Text);
                sw.Close();
                textBox1.Clear();
            }


        }

        private void textColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColorDialog dlg = new ColorDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                textBox1.ForeColor = dlg.Color;

            }
        }

        private void backgroundColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColorDialog dlg = new ColorDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                textBox1.BackColor = dlg.Color;
            }
        }

        private void fontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FontDialog dlg = new FontDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
                contextMenuStrip1.SourceControl.Font = dlg.Font;
        }



        private void button1_Click(object sender, EventArgs e)
        {

            //pictureBox1.ImageLocation = lista2[0].Path;
            //textBox1.Clear();

            //textBox1.Text += lista2[0].ToString();
            //pictureBox1.Image = null;

            textBox1.Clear();
            OleDbConnection conexiune = new OleDbConnection(Provider);
            OleDbDataAdapter adaptor = new OleDbDataAdapter("SELECT * FROM produs", conexiune);
            DataSet dataSet = new DataSet();
            adaptor.Fill(dataSet, "produs");

            DataTable tabela = dataSet.Tables["produs"];

            DataRow linie = tabela.Rows[0];
            //foreach (object camp in linie.ItemArray)
            //    textBox1.Text += camp + "     ";
            //textBox1.Text += Environment.NewLine;



            textBox1.Text += "Produsul cu id-ul " + linie["ID"] + " este " + linie["Denumire"] + Environment.NewLine
               + "Produsul expira la data de " + linie["DataExpirare"] + Environment.NewLine
               + "Numar bucati disponibile: " + linie["NumarBucati"] + Environment.NewLine;




            textBox1.Text += Environment.NewLine + Environment.NewLine + "INFORMATII NUTRITIONALE: " + Environment.NewLine + linie["Proteine"] + " grame proteine, " + linie["Lipide"] + " grame lipide, " + linie["Glucide"] + " grame glucide. Valoare energetica (kcal): " + linie["Kcal"] + " kcal.";
            textBox1.Text += Environment.NewLine + Environment.NewLine;
            if (Convert.ToDouble(linie["Pret_fabricatie"]) != 0 && Convert.ToDouble(linie["Pret_vanzare"]) != 0)
                textBox1.Text += Environment.NewLine + "Pret fabricatie: " + linie["Pret_fabricatie"] + " lei. Pret vanzare: " + linie["Pret_vanzare"] + " lei.";
            else
                textBox1.Text += "Momentan pretul de productie si pretul de vanzare pentru acest produs nu sunt disponibile intrucat nu au fost introduse materiile prime necesare prepararii";

            textBox1.Text += Environment.NewLine + Environment.NewLine + Environment.NewLine;
            pictureBox1.ImageLocation =linie["PATH"].ToString(); 
        }

        int i = 0;
        int nr ;

        private void bunifuThinButton21_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            OleDbConnection conexiune = new OleDbConnection(Provider);
            OleDbDataAdapter adaptor = new OleDbDataAdapter("SELECT * FROM produs", conexiune);
            DataSet dataSet = new DataSet();
            adaptor.Fill(dataSet, "produs");

            DataTable tabela = dataSet.Tables["produs"];
            i++; 
            nr= tabela.Rows.Count;

           
            try
            {
                if(i<=nr)
                {
                     DataRow linie = tabela.Rows[i];
                    textBox1.Text += "Produsul cu id-ul " + linie["ID"] + " este " + linie["Denumire"] + Environment.NewLine
              + "Produsul expira la data de " + linie["DataExpirare"] + Environment.NewLine
              + "Numar bucati disponibile: " + linie["NumarBucati"] + Environment.NewLine;

                    textBox1.Text += Environment.NewLine + Environment.NewLine + "INFORMATII NUTRITIONALE: " + Environment.NewLine + linie["Proteine"] + " grame proteine, " + linie["Lipide"] + " grame lipide, " + linie["Glucide"] + " grame glucide. Valoare energetica (kcal): " + linie["Kcal"] + " kcal.";
                    textBox1.Text += Environment.NewLine + Environment.NewLine;


                    if (Convert.ToDouble(linie["Pret_fabricatie"]) != 0 && Convert.ToDouble(linie["Pret_vanzare"]) != 0)
                        textBox1.Text += Environment.NewLine + "Pret fabricatie: " + linie["Pret_fabricatie"] + " lei. Pret vanzare: " + linie["Pret_vanzare"] + " lei.";
                    else
                        textBox1.Text += "Momentan pretul de productie si pretul de vanzare pentru acest produs nu sunt disponibile intrucat nu au fost introduse materiile prime necesare prepararii";

                    textBox1.Text += Environment.NewLine + Environment.NewLine + Environment.NewLine;
                    pictureBox1.ImageLocation = linie["PATH"].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Nu mai exista produse de afisat");
                pictureBox1.Image = null; 
            }


        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
   


    



