﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROIECTpaw
{
    public class MateriePrima
    {
        string denumireMateriePrima;
        float cantitateMateriePrima;
        float pretMateriePrima;

        public string DenumireMateriePrima
        {
            get
            {
                return this.denumireMateriePrima;
            }

            set
            {
                this.denumireMateriePrima = value;
            }
        }
        public float CantitateMateriePrima
        {
            get
            {
                return this.cantitateMateriePrima;
            }

            set
            {
                this.cantitateMateriePrima = value;
            }
        }
        public float PretMateriePrima
        {
            get
            {
                return this.pretMateriePrima;
            }

            set
            {
                this.pretMateriePrima= value;
            }
        }
        public MateriePrima(string denumireMateriePrima, float cantitateMateriePrima, float pretMateriePrima)
        {
            this.denumireMateriePrima = denumireMateriePrima;
            this.cantitateMateriePrima = cantitateMateriePrima;
            this.pretMateriePrima = pretMateriePrima;
        }
        public override string ToString()
        {
            string result = null;
            result += "A fost adaugata materia prima " + this.denumireMateriePrima + " cu cantitatea de " + this.cantitateMateriePrima + " grame. Pret/kg: "+this.pretMateriePrima+" lei.";
            return result;
        }
    }
}
