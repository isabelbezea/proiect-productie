﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace PROIECTpaw
{
    public partial class Form4 : Form
    {
        List<LotFabricatie> lista4 = new List<LotFabricatie>();
        private string numar = null;
        
        private int ziuaDinAn;
        public Form4()
        {
            InitializeComponent();
            
        }
        

        
        private void numarLotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Numarul lotului este " + numar);
        }

      

       

       

        private void bunifuThinButton21_Click(object sender, EventArgs e)
        {
            if (tbDen.Text == "")
                errorProvider1.SetError(tbDen, "Denumirea produsului trebuie introdusa!");
            else
                if (tbNr.Text == "")
                errorProvider1.SetError(tbNr, "Numarul liniei de productie trebuie introdus!");

            OleDbConnection conexiune = new OleDbConnection("Provider = Microsoft.ACE.OLEDB.12.0; Data Source = produse.accdb");
            try
            {

                conexiune.Open();
                OleDbCommand comanda = new OleDbCommand();
                comanda.Connection = conexiune;

                comanda.CommandText = " SELECT MAX(ID) FROM LotFabricatie";
                int id = Convert.ToInt32(comanda.ExecuteScalar());
                comanda.CommandText = "INSERT INTO LotFabricatie VALUES(?,?,?,?,?)";
                comanda.Parameters.Add("ID", OleDbType.Integer).Value = id + 1;
                comanda.Parameters.Add("DenumireProdus", OleDbType.Char, 20).Value = tbDen.Text;
                comanda.Parameters.Add("NumarLinieProductie", OleDbType.Integer).Value = Convert.ToInt32(tbNr.Text);
                comanda.Parameters.Add("DataProductie", OleDbType.Date).Value = dateTimePicker1.Value.ToShortTimeString();
                if (radioButtonRomania.Checked == true)
                    comanda.Parameters.Add("TaraProvenienta", OleDbType.Char, 20).Value = radioButtonRomania.Text;
                else
                    comanda.Parameters.Add("TaraProvenienta", OleDbType.Char, 20).Value = radioButtonFranta.Text;

                comanda.ExecuteNonQuery(); 

                DateTime data = dateTimePicker1.Value;
                int an = data.Year;
                int ziua = data.Day;
                int luna = data.Month;

                int nr = Convert.ToInt32(tbNr.Text);
                string tara = null;
                if (radioButtonRomania.Checked == true)
                    tara = radioButtonRomania.Text;
                else if (radioButtonFranta.Checked == true)
                    tara = radioButtonFranta.Text;


                

                if (an % 4 != 0) //anul nu este bisect deci februarie are 28 de zile
                {
                    if (luna == 01)
                        ziuaDinAn = ziua;
                    else if (luna == 02)
                        ziuaDinAn = 31 + ziua;
                    else if (luna == 03)
                        ziuaDinAn = 59 + ziua;
                    else if (luna == 04)
                        ziuaDinAn = 90 + ziua;
                    else if (luna == 05)
                        ziuaDinAn = 120 + ziua;
                    else if (luna == 06)
                        ziuaDinAn = 151 + ziua;
                    else if (luna == 07)
                        ziuaDinAn = 181 + ziua;
                    else if (luna == 08)
                        ziuaDinAn = 212 + ziua;
                    else if (luna == 09)
                        ziuaDinAn = 243 + ziua;
                    else if (luna == 10)
                        ziuaDinAn = 273 + ziua;
                    else if (luna == 11)
                        ziuaDinAn = 304 + ziua;
                    else if (luna == 12)
                        ziuaDinAn = 334 + ziua;
                }
                if (an % 4 == 0) //anul  este bisect deci februarie are 29 de zile
                {
                    if (luna == 01)
                        ziuaDinAn = ziua;
                    else if (luna == 02)
                        ziuaDinAn = 31 + ziua;
                    else if (luna == 03)
                        ziuaDinAn = 60 + ziua;
                    else if (luna == 04)
                        ziuaDinAn = 91 + ziua;
                    else if (luna == 05)
                        ziuaDinAn = 121 + ziua;
                    else if (luna == 06)
                        ziuaDinAn = 152 + ziua;
                    else if (luna == 07)
                        ziuaDinAn = 182 + ziua;
                    else if (luna == 08)
                        ziuaDinAn = 213 + ziua;
                    else if (luna == 09)
                        ziuaDinAn = 244 + ziua;
                    else if (luna == 10)
                        ziuaDinAn = 274 + ziua;
                    else if (luna == 11)
                        ziuaDinAn = 304 + ziua;
                    else if (luna == 12)
                        ziuaDinAn = 335 + ziua;
                }

                //format lot: initiala tarii de provenienta - litera L - numarul lotului - ultimele 2 cifre din an - numarul zilei din an
                numar = tara[0] + "L" + nr + (an % 100) + ziuaDinAn;




                
                MessageBox.Show( "Data introdusa de dvs reprezinta ziua numarul " + ziuaDinAn + " din an");
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
            finally
            {
                tbDen.Text = "";
                tbNr.Text="";

                radioButtonRomania.Checked = false;
                radioButtonFranta.Checked = false;
                errorProvider1.Clear();
                conexiune.Close();
            }


        }

        private void tbNr_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
                e.Handled = true;
        }
    }
    }
    

