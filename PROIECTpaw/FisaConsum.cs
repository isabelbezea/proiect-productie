﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROIECTpaw
{
   public  class FisaConsum:ICloneable
    {
        string denumireProdus;
        int n; //numar Materii Prime folosite
        string[]denumiriMatPrima;
        float[] cantitatiMatPrima;
        float[] preturiMatPrima;
       // float pretProductie;

        public FisaConsum(string denumireProdus, int n, string[] denumiriMatPrima, float[] cantitatiMatPrima, float[] preturiMatPrima)
        {
            this.denumireProdus = denumireProdus;
            this.n = n;
            this.denumiriMatPrima = new string[n];
            for (int i = 0; i < n; i++)
                this.denumiriMatPrima[i] = denumiriMatPrima[i];

            this.cantitatiMatPrima = new float[n];
            for (int i = 0; i < n; i++)
                this.cantitatiMatPrima[i] = cantitatiMatPrima[i];

            this.preturiMatPrima = new float[n];
            for (int i = 0; i < n; i++)
                this.preturiMatPrima[i] = preturiMatPrima[i];
            //this.pretProductie = pretProductie;

        }
        public string DenumireProdus
        {
            get
            {
                return this.denumireProdus;
            }
            set
            {
                this.denumireProdus = value;
            }
        }
        public int Nr
        {
            get
            {
                return this.n;
            }
            set
            {
                this.n = value;
            }
        }
        public string[] DenumiriMatPrima
        {
            get
            {
                return this.denumiriMatPrima;
            }
            set
            {
                this.denumiriMatPrima = value;
            }
        }
        public float[] CantitatiMatPrima
        {
            get
            {
                return this.cantitatiMatPrima;
            }
            set
            {
                this.cantitatiMatPrima = value;
            }
        }

        public float[] PreturiMatPrima
        {
            get
            {
                return this.preturiMatPrima;
            }
            set
            {
                this.preturiMatPrima = value;
            }
        }
        //public float PretProductie
        //{
        //    get
        //    {
        //        return this.pretProductie;
        //    }
        //    set
        //    {
        //        this.pretProductie = value;
        //    }
        //}
        public object Clone()
        {
            FisaConsum fs = (FisaConsum)this.MemberwiseClone();

            string[] copieDenumiriMatPrima = (string[])this.denumiriMatPrima.Clone();
            fs.denumiriMatPrima = copieDenumiriMatPrima;

            float[] copieCantitatiMatPrima = (float[])this.cantitatiMatPrima.Clone();
            fs.cantitatiMatPrima = copieCantitatiMatPrima;
            return fs;
        }

     
        public override string ToString()
        {
            string result = null;
            result += "Pentru produsul cu denumirea " + this.denumireProdus+ " au fost necesare " + this.n + " materii prime: ";
                for (int i = 0; i < this.n - 1; i++)
                    result += this.cantitatiMatPrima[i] + " grame de " + this.denumiriMatPrima[i] + ", ";
                result += this.cantitatiMatPrima[this.n - 1] + " grame de " + this.denumiriMatPrima[this.n - 1]+".";
           // result += "Pret productie: " + this.pretProductie + " lei.";
            return result;



        }
        
    }
}

