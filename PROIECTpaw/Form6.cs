﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
namespace PROIECTpaw
{
    public partial class Form6 : Form
    {
        string connString;
        
        public Form6()
        {
            InitializeComponent();
            connString = "Provider = Microsoft.ACE.OLEDB.12.0; Data Source = produse.accdb";
            OleDbConnection conexiune = new OleDbConnection(connString);

            chart1.ChartAreas["ChartArea1"].AxisX.MajorGrid.LineWidth = 0;
            chart1.ChartAreas["ChartArea1"].AxisY.MajorGrid.LineWidth = 0;
            chart1.Titles.Add("Proportie valori nutritionale");

            try {
                conexiune.Open();
                OleDbCommand comanda = new OleDbCommand();
                comanda.CommandText = "SELECT * FROM produs";
                comanda.Connection = conexiune;
                OleDbDataReader reader = comanda.ExecuteReader();
                while (reader.Read())
                {

                    chart1.Series["Proteine"].Points.AddXY(reader["Denumire"], reader["Proteine"]);
                    chart1.Series["Lipide"].Points.AddXY(reader["Denumire"], reader["Lipide"]);
                    chart1.Series["Glucide"].Points.AddXY(reader["Denumire"], reader["Glucide"]);
                }  }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
               
            }
            finally
            {
                conexiune.Close();

            }


        }


        
    }
}
