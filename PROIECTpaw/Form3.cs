﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Printing;
using System.Data.OleDb;
//prin acest formular, se introduc de la tastatura materiile prime si cantitatile corespunzatoare si le  adauga la  informatii pentru produs
namespace PROIECTpaw
{
    public partial class Form3 : Form
    {
        string Provider;

        MateriePrima[] obMatPrime;


        public Form3()
        {
            InitializeComponent();
            Provider = "Provider = Microsoft.ACE.OLEDB.12.0; Data Source = produse.accdb";

        }


        private void saveFileToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "(*.txt)|*.txt";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(dlg.FileName);
                sw.WriteLine(textBox1.Text);
                sw.Close();
                textBox1.Clear();
            }
        }






        private void btnAdauga_Click(object sender, EventArgs e)
        {
            if (tbDen.Text == "")
                errorProvider1.SetError(tbDen, "Denumirea produsului trebuie introdusa!");
            else
                if (tbNr.Text == "")
                errorProvider1.SetError(tbNr, "Numarul de materii prime folosite trebuie introdus!");
            else
                if (tbMat.Text == "")
                errorProvider1.SetError(tbMat, "Materiile prime trebuie introduse!");
            else
                if (tbCant.Text == "")
                errorProvider1.SetError(tbCant, "Cantitatile de materie prima folosite trebuie introduse!");
            OleDbConnection conexiune = new OleDbConnection("Provider = Microsoft.ACE.OLEDB.12.0; Data Source = produse.accdb");
            try
            {

                conexiune.Open();
                OleDbCommand comanda = new OleDbCommand();
                //OleDbCommand comanda1 = new OleDbCommand();
                comanda.Connection = conexiune;
                //comanda1.Connection = conexiune;

                //comanda.CommandText = "SELECT IDMateriePrima from MateriePrima WHERE Denumire=ala_de_la_tastatura";
                //int id_materie_prima = Convert.ToInt32(comanda.ExecuteScalar());
                int nr = Convert.ToInt32(tbNr.Text);
                int i = 0;
                while (i < nr)
                {
                    try
                    {
                     

                        
                            comanda.CommandText = " SELECT MAX(IDMateriePrima) FROM MateriePrima";
                            int id = Convert.ToInt32(comanda.ExecuteScalar());
                            comanda.CommandText = "INSERT INTO MateriePrima VALUES(?,?,?)";
                            comanda.Parameters.Add("IDMateriePrima", OleDbType.Integer).Value = id + 1;

                            comanda.Parameters.Add("DenumireMateriePrima", OleDbType.Char, 20).Value = tbMat.Text.Split(',')[i];

                            comanda.Parameters.Add("PretPerKgMateriePrima", OleDbType.Double).Value = Convert.ToDouble(tbPret.Text.Split(',')[i]);
                            comanda.ExecuteNonQuery();
                            MessageBox.Show("Materia prima a fost adaugata cu succes in baza de date!");
                            i++;
                        
                    }
                    catch
                    {
                        i++;
                        

                    }
                }

                for (int j = 0; j < nr; j++)
                {
                    comanda.Parameters.Clear();
                    comanda.CommandText = " SELECT IDMateriePrima from MateriePrima WHERE DenumireMateriePrima='" + tbMat.Text.Split(',')[j]+"'";
                    int idMatPrima = Convert.ToInt32(comanda.ExecuteScalar());

                    comanda.CommandText = " SELECT ID from produs WHERE Denumire='" + tbDen.Text+"'";
                    int idProd = Convert.ToInt32(comanda.ExecuteScalar());

                    comanda.CommandText = "INSERT INTO TabelaIntermediara VALUES(?,?)";
                    comanda.Parameters.Add("ID", OleDbType.Integer).Value = idProd;
                    comanda.Parameters.Add("IDMateriePrima", OleDbType.Integer).Value = idMatPrima;
                    comanda.ExecuteNonQuery();
                }

             

                double pretFabricatie = 0;
                double pretVanzare = 0;
                for (int j = 0; j < nr; j++)

                    pretFabricatie += Convert.ToDouble(tbPret.Text.Split(',')[j]) * (Convert.ToDouble(tbCant.Text.Split(',')[j])/1000);

           
                pretVanzare = pretFabricatie * 2;
                string den = tbDen.Text;
                comanda.CommandText = " UPDATE produs SET Pret_fabricatie=" + pretFabricatie + " WHERE Denumire = '" + den+"'";
                
                comanda.ExecuteNonQuery();
                comanda.CommandText = " UPDATE produs SET Pret_vanzare= " + pretVanzare + " WHERE Denumire = '" + den+"'";
                comanda.ExecuteNonQuery();
            }

           


            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
            finally
            {
                tbDen.Text = "";
                tbNr.Text = "";
                tbMat.Text = "";
                tbCant.Text = "";
                tbPret.Text = "";
                errorProvider1.Clear();
                conexiune.Close(); 
            }

        }

        private void bunifuThinButton21_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            OleDbConnection conexiune = new OleDbConnection(Provider);
            OleDbDataAdapter adaptor = new OleDbDataAdapter("SELECT * FROM produs", conexiune);
            OleDbDataAdapter adaptor1 = new OleDbDataAdapter("SELECT * FROM MateriePrima", conexiune);

            DataSet dataSet = new DataSet();
            DataSet dataSet1 = new DataSet();

            adaptor.Fill(dataSet, "produs");
            adaptor1.Fill(dataSet1, "MateriePrima");


            DataTable tabela = dataSet.Tables["produs"];
            DataTable tabela1 = dataSet1.Tables["MateriePrima"];

            OleDbDataAdapter adaptor2 = new OleDbDataAdapter("SELECT * FROM TabelaIntermediara", conexiune);
            DataSet dataSet2 = new DataSet();
            adaptor2.Fill(dataSet2, "TabelaIntermediara");
            DataTable tabela2 = dataSet2.Tables["TabelaIntermediara"];

            

            foreach (DataRow linie in tabela.Rows)
            {
              

                textBox1.Text += "Produsul cu id-ul " + linie["ID"] + " este " + linie["Denumire"] + Environment.NewLine
               + "Produsul expira la data de " + linie["DataExpirare"] + Environment.NewLine
               + "Numar bucati disponibile: " + linie["NumarBucati"] + Environment.NewLine;

                textBox1.Text += Environment.NewLine + Environment.NewLine + "INFORMATII NUTRITIONALE: " + Environment.NewLine + linie["Proteine"] + " grame proteine, " + linie["Lipide"] + " grame lipide, " + linie["Glucide"] + " grame glucide. Valoare energetica (kcal): " + linie["Kcal"] + " kcal.";
                textBox1.Text += Environment.NewLine + Environment.NewLine;
                textBox1.Text += Environment.NewLine + "MATERII PRIME NECESARE: " + Environment.NewLine;

                DataRow[] rows = tabela2.Select("ID="+linie["ID"]);
                foreach (DataRow l in rows)
                    foreach (DataRow linie1 in tabela1.Rows)

                        if (Convert.ToInt32(linie1["IDMateriePrima"]) == Convert.ToInt32(l["IDMateriePrima"]))
                        {

                            textBox1.Text += linie1["DenumireMateriePrima"] + "; "; 

                        }


                
                if (Convert.ToDouble(linie["Pret_fabricatie"]) != 0 && Convert.ToDouble(linie["Pret_vanzare"]) != 0)
                    textBox1.Text += Environment.NewLine + "Pret fabricatie: " + linie["Pret_fabricatie"] + " lei. Pret vanzare: " + linie["Pret_vanzare"] + " lei.";
                else
                    textBox1.Text += "Momentan pretul de productie si pretul de vanzare pentru acest produs nu sunt disponibile intrucat nu au fost introduse materiile prime necesare prepararii";

               
                textBox1.Text += Environment.NewLine + Environment.NewLine + Environment.NewLine;
                textBox1.Text += "------------------------------------"+Environment.NewLine;
                
            }
        }
    

        private void tbNr_Validating_1(object sender, CancelEventArgs e)
        {
            try
            {
                float pret = Convert.ToInt32(tbNr.Text);
                if (pret == 0)
                    MessageBox.Show("Numarul de materii prime nu poate fi 0!");
            }
            catch (FormatException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void tbNr_KeyPress_2(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void tbCant_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) &&
            !char.IsControl(e.KeyChar) && e.KeyChar != ',' && e.KeyChar != '.')
                e.Handled = true;
        }

        private void tbPret_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) &&
             !char.IsControl(e.KeyChar) && e.KeyChar != ',')
                e.Handled = true;
        }

       

        //private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        //{
        //    e.Graphics.DrawString(textBox1.Text, new Font("Century Gothic", 30, FontStyle.Regular), Brushes.Black, 150, 125);
        //}
        private void pdPrint(object sender, PrintPageEventArgs e)
        {
         
            e.Graphics.DrawString(textBox1.Text, new Font("Century Gothic", 11, FontStyle.Regular), Brushes.Black, 150, 125);
        
        }

        private void bunifuThinButton22_Click(object sender, EventArgs e)
        {
            PrintDocument pd = new PrintDocument();
            pd.PrintPage += new PrintPageEventHandler(pdPrint);

            PrintPreviewDialog dlg = new PrintPreviewDialog();
            dlg.Document = pd;
            dlg.ShowDialog();
        }
    }
    
    }

