# Proiect Productie 

A C# Project managing the entire line of production of a meat factory. The project incorporates editable C# forms, keyboard-shortcuts, drag&drop, whilst all the data is stored in an Access database. 

In the next lines, I will try to show you how the application works, step by step.

This is the start page of my project
![1](/uploads/724cea65afedce7c0f5468354b11e9e5/1.png) 

Accessing the first option of the menu, a new form will be opened. Here, by clicking the highlighted option, we have the possibility to see the data stored in the **database** using a **listview**.
![1](/uploads/49629e89e9f258f1c96013d96ccc8845/1.png) 
![2](/uploads/31b473bae91d649488a0d55bd37bec3e/2.png) 

Also here, we can add new products in the database, filling the required fields (I made sure validations are in place, so that mandatory data gets filled in). Every product should have an image associated and we do that by simply **drag&drop** the image into the listbox. I made sure that a copy of image is automatically created in the project folder so that in case the image was added from another location and at some point, it gets moved/deleted, everything would still work as planned. When we are all set up, we add the product by clicking the "Adauga produs" button and a confirmation message will inform us if the product was successfully inserted in the database or  if any problems occured. Also, by clicking the insertion button, the energy value of the product will be calculated depending on the values of proteins, carbohydrates, lipids we enter.
![2](/uploads/b52b6d72f5adce67df2450827d889e38/2.png)



We also have the possibility to view charts regarding nutritional informations of the products.
![2](/uploads/6350fcd12d428085d6f0aff6924447cf/2.png) 


Accessing the third option of the menu, it is opened the form by which we enter the raw material of the products already stored in the database. Depending of the entered values, it is automatically calculated the production & sell price of the product, following that these prices to be updated in the database (initially they were stored with the 0 value).
![2](/uploads/054f0a69ee375a48e10d0e1118ecbdb8/2.png)
Also here, we have the possibility to **print preview** the list of products, save as a txt file etc.


Accessing the second option of the menu, we can view every product stored in the database in a more friendly way (description + image), one by one, using the "Urmatorul produs" button to slide over them.

![2](/uploads/954f3bdeebb055e3a9fdc84721ca67c3/2.png) 

The product from the above picture has the fabrication & sell price calculated, but in case of a product with no data available regarding the used raw materials, there will be a corresponding message.

Last but not least, the last option of the menu creates the manufacturing batches.
![2](/uploads/e09df95f080eac2baa4cc6018d66aee9/2.png) 

Thank you for your attention!


